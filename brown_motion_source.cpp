#include <iostream>
#include <cstring>
#include <time.h>
#include <math.h>
#include <cstdlib>
#include "simple_svg_1.0.0.hpp"
#include <string>

class Brown_trajectory {
private:
	double * B;
	bool cond;
public:
	Brown_trajectory();
	Brown_trajectory(double * timeDisc, int dim, double(*NormDist)(double, double));
	double getB(int i);
	void condition(bool b);
	bool getCond();
};

Brown_trajectory::Brown_trajectory()
{
}

/**
	Generates trajectory of Brownian motion on timeDisc.
*/
Brown_trajectory::Brown_trajectory(double * timeDisc, int dim, double(*NormDist)(double, double))
{
	double * increments = new double[dim];

	for (int i = 0; i < dim; i++) {
		increments[i] = (*NormDist)(0, timeDisc[i + 1] - timeDisc[i]);
	}

	B = new double[dim+1];

	B[0] = 0;
	for (int i = 0; i < dim; i++)
	{
		B[i + 1] = B[i] + increments[i];
	}

}

double Brown_trajectory::getB(int i)
{
	return B[i];
}

void Brown_trajectory::condition(bool b)
{
	cond = b;
}

bool Brown_trajectory::getCond()
{
	return cond;
}

/**
	Generates random number following Normal distribution. Using Box-Muller method.
*/
double generateNormalDist(double mu, double sigma) {
	static const double two_pi = 2.0*3.14159265358979323846;

	double u1 = rand() * (1.0 / RAND_MAX);
	double u2 = rand() * (1.0 / RAND_MAX);

	double z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);

	return z0 * sigma + mu;
}

/**
	Makes .svg image of generated trajectories of Brownian motion.
*/
void printTrajectoriesSVG(double * timeDisc, std::vector<Brown_trajectory> trajectories, int dim) {
	using namespace svg;
	
	struct axes {
		static void printAxes(int dimX, int dimY, Document &doc)
		{
			Polyline axisX(Stroke(1, Color::Black));
			Polyline axisY(Stroke(1, Color::Black));

			axisX << Point(10, dimY / 2) << Point(dimX, dimY / 2);
			axisY << Point(10, 10) << Point(10, dimY - 10);
			doc << axisX << axisY;

			double s = (dimX - 20) / 10;
			for (int i = 0; i < 11; i++) {
				doc << Text(Point(10 + i * s, dimY / 2 - 10), i, Color::Black, Font(10, "Verdana"));
			}
		}
	};
	
	int dimX = 1000;
	int dimY = 1000;

	Dimensions dimensions(dimX, dimY);
	Document doc("brown_motions_graph.svg", Layout(dimensions, Layout::BottomLeft));

	axes::printAxes(dimX, dimY, doc);

	double x1, y1, x2, y2;
	double scalex = 100;
	double scaley = 100;
	Color c(0,0,0);
	for (int t = 0; t < trajectories.size(); t++) {
		if (trajectories[t].getCond()) {
			c = Color::Red;
		}
		else {
			c = Color::Blue;
		}

		Polyline line(Stroke(1, c));
		for (int i = 0; i < dim; i++) {
			x1 = timeDisc[i] * scalex + 10;
			x2 = timeDisc[i + 1] * scalex + 10;
			y1 = trajectories[t].getB(i) * scaley + dimY / 2;
			y2 = trajectories[t].getB(i + 1) * scaley + dimY / 2;

			line << Point(x1, y1) << Point(x2, y2);
			
		}

		doc << line;
	}

	axes::printAxes(dimX, dimY, doc);

	doc.save();
}

/**
	Chceks if the trajectory satisfies choosen condition
*/
bool checkCondition(Brown_trajectory &B, double * timeDisc, int dim) {
	int t1 = dim / 10;
	int t2 = t1 * 2;

	double e = 1 / dim;

	if (B.getB(t2) > (3+sqrt(3)) * B.getB(t1) && B.getB(t2) > (-1 - sqrt(3)) * B.getB(t1)) {
		B.condition(true);
		return true;
	}
	else {
		B.condition(false);
		return false;
	}
}

// necessary time disc. points
double t1 = 1;
double t2 = 2;

/**
	Generates random discretization.
*/
double * random_time_discretization(int N) {
	double * timeDisc = new double[N+1];

	double step = 10.0 / N;
	double d = 0;
	for (int i = 0; i < N + 1; i++) {
		d = ((double)rand() / (RAND_MAX)) * 2 - 1;
		d *= 1.0 / (N * 100);
		
		timeDisc[i] = i * step + d;
	}

	timeDisc[0] = 0;

	timeDisc[N / 10] = t1;
	timeDisc[N / 10 * 2] = t2;

	return timeDisc;
}

void print_random_vectors(std::vector<Brown_trajectory> trajectories, int dim) {
	t1 = dim / 10;
	t2 = 2 * t1;

	int dimX = 1000;
	int dimY = 1000;

	using namespace svg;

	Dimensions dimensions(dimX, dimY);
	Document doc("BM_random_vector_graph.svg", Layout(dimensions, Layout::BottomLeft));

	struct axes {
		static void printAxes(int dimX, int dimY, Document &doc)
		{
			Polyline axisX(Stroke(1, Color::Black));
			Polyline axisY(Stroke(1, Color::Black));

			axisX << Point(10, dimY / 2) << Point(dimX, dimY / 2);
			axisY << Point(dimX / 2, 10) << Point(dimX / 2, dimY - 10);
			doc << axisX << axisY;

			for (int i = 0; i < 10; i++) {
				doc << Text(Point(dimX / 2 + i * 50, dimY / 2 - 10), std::to_string(i), Color::Black, Font(10, "Verdana"));
				doc << Text(Point(dimX / 2 - i * 50, dimY / 2 - 10), std::to_string(-i), Color::Black, Font(10, "Verdana"));
				doc << Text(Point(dimX / 2 - 10, dimY / 2 + (i + 1) * 50), std::to_string(i + 1), Color::Black, Font(10, "Verdana"));
				doc << Text(Point(dimX / 2 - 10, dimY / 2 - (i + 1) * 50), std::to_string(-(i + 1)), Color::Black, Font(10, "Verdana"));
			}
		}
	};

	axes::printAxes(dimX, dimY, doc);

	bool condition;
	Color c(0,0,0);
	double x, y;

	for (int i = 0; i < trajectories.size(); i++) {
		if (trajectories[i].getCond()) {
			c = Color::Red;
		}
		else {
			c = Color::Blue;
		}

		x = trajectories[i].getB(t1) * 50 + dimX / 2;
		y = trajectories[i].getB(t2) * 50 + dimY / 2;

		doc << Circle(Point(x, y), 5, Fill(c), Stroke(1, c));
	}

	axes::printAxes(dimX, dimY, doc);

	doc.save();
}

int main() {
	srand(time(NULL));

	int N; // number of points
	int numOfTraj;

	std::cout << "Zadaj pocet bodov diskretizacie: ";
	std::cin >> N;
	std::cout << "Zadaj pocet generovanych trajektorii: ";
	std::cin >> numOfTraj;

	/* TIME DISCRETIZATION */
	double * timeDisc = random_time_discretization(N);
	

	/* BROWN TRAJECTORIES */
	std::vector<Brown_trajectory> trajectories;
	for (size_t i = 0; i < numOfTraj; i++)
	{
		trajectories.push_back(Brown_trajectory(timeDisc, N, generateNormalDist));
	}

	/* CONTROL THE CONDITION */
	int condSatisfied = 0;
	for (rsize_t i = 0; i < numOfTraj; i++) {
		if (checkCondition(trajectories[i], timeDisc, N)) {
			condSatisfied++;
		}
	}

	std::cout << "Experimentalny odhad pravdepodobnosti je " << (double)condSatisfied / numOfTraj << "." << std::endl;
	std::cout << "Chces vykreslit graf? (Y/N)" << std::endl;
	
	char A;
	std::cin >> A;
	while (A != 'Y' && A != 'N') {
		std::cout << "Zly znak. Zadaj Y pre vykreslenie, alebo N ak nechces vykreslit." << std::endl;
		std::cin >> A;
	}
	if (A == 'Y') {
		printTrajectoriesSVG(timeDisc, trajectories, N);
		print_random_vectors(trajectories, N);
		std::cout << "Graf je vykresleny." << std::endl;
	}

	system("PAUSE");

}
